# Intro

This is 8x8 Othello move generator, using PDEP/PEXT bitboard. 

# Move generation

The move generation part uses the Kogge-Stone Algorithm, intended to compile to SIMD operations.

# Move resolution

The move resolution part uses PDEP/PEXT bitboard, with 18.5KiB of LUT, intended to fit in the L1D cache. 
