#![cfg(test)]

use super::*;

#[test]
fn default_test() {
    let x = Game::default();
    assert_eq!(x.available_moves(), 0x0000_1020_0408_0000);
    let x = x.make_move(44).unwrap();
    assert_eq!(x.get(), [0x0000_0000_0800_0000, 0x0000_1018_1000_0000]);
}

#[test]
fn range_test() {
    for i in 0..4 {
        for place in 0..64 {
            assert!(consts::MASK[place][i][0].count_ones() <= 6);
            assert!(consts::MASK[place][i][1].count_ones() <= 6);
        }
    }
}

#[must_use]
fn perft(g: &Game, depth: usize) -> u64 {
    match depth {
        0 => 1,
        //1 => u64::from(g.available_moves().count_ones()),
        _ => match g.available_moves() {
            0 => {
                let mut g = g.clone();
                g.pass_move();
                perft(&g, depth - 1)
            }
            r => bititr::BitScan(r)
                .map(|x| perft(&g.clone().make_move(x as usize).unwrap(), depth - 1))
                .sum::<u64>(),
        },
    }
}
// panic!(
//     "{:?}\n {:?} {}\n{:?}",
//     g.get(),
//     g,
//     x,
//     [
//         g.gen(9),
//         g.gen(8),
//         g.gen(7),
//         g.gen(1),
//         g.gen(-1),
//         g.gen(-7),
//         g.gen(-8),
//         g.gen(-9)
//     ]
// )
// }),
#[test]
fn perf_test() {
    let x = Game::default();
    let arr = [
        1,
        4,
        12,
        56,
        244,
        1396,
        8200,
        55092,
        390_216,
        3_005_288,
        24_571_284,
        212_258_800,
        1_939_886_636,
    ];
    for (depth, num) in arr.iter().enumerate() {
        assert_eq!(perft(&x, depth), *num);
    }
}
