pub const RIGHTY: u64 = 0x7F7F_7F7F_7F7F_7F7F;
pub const LEFTY: u64 = 0xFEFE_FEFE_FEFE_FEFE;
const HELPER: [u16; 8] = [0, 0o200, 0o201, 0o300, 0o301, 0o400, 0o401, 0o500];
pub fn mask(position: usize) -> [[u64; 2]; 4] {
    const MID: u64 = !0 << 8 >> 16 << 8;
    const CENTER: u64 = MID & LEFTY & RIGHTY;
    let t: u64 = 1 << position;
    let a = t | t << 8 | t >> 8;
    let a = a | a << 1 & LEFTY | a >> 1 & RIGHTY;
    let mut re: [[u64; 2]; 4] = Default::default();
    for (i, re) in re.iter_mut().enumerate() {
        let mut r = [0xFF, !0 / 0xFF, 0x80, 1][i];
        while r & t == 0 {
            r = [
                r << 8,
                r << 1,
                r >> 1 & RIGHTY | r << 8,
                r << 1 & LEFTY | r << 8,
            ][i];
        }
        re[0] = r & !a;
        re[1] = r & [RIGHTY & LEFTY, MID, CENTER, CENTER][i] & !t;
    }
    re
}
pub fn index(position: usize) -> [u16; 4] {
    let a = position & 7;
    let b = position >> 3;
    [
        HELPER[a],
        HELPER[b],
        HELPER[core::cmp::min(a, b)],
        HELPER[core::cmp::min(7 - a, b)],
    ]
}
//can we remove some checks and rely on miri test?
pub fn result_array() -> [u8; 0x3800] {
    const NOT_INITIALIZED: u8 = !0;
    let mut arr = [NOT_INITIALIZED; 0x3800];

    for (ii, &i) in HELPER.iter().enumerate() {
        let rangemax = match ii {
            0 | 7 => 64,
            _ => 32,
        };
        for j in 0..rangemax {
            for k in 0..rangemax {
                use bitintr::*;

                let i = i as usize * 32 + j * 64 + k;
                let jj = (j as u64).pdep(mask(ii)[0][0]);
                let kk = (k as u64).pdep(mask(ii)[0][1]);

                assert_eq!(jj.pext(mask(ii)[0][0]), j as u64);
                assert_eq!(kk.pext(mask(ii)[0][1]), k as u64);

                assert_eq!(arr[i], NOT_INITIALIZED);
                assert_eq!(result(ii, jj, kk) & !mask(ii)[0][1], 0);
                arr[i] = core::convert::TryInto::try_into(result(ii, jj, kk).pext(mask(ii)[0][1]))
                    .unwrap();
            }
        }
    }
    for &i in &arr[..] {
        assert_ne!(i, NOT_INITIALIZED);
    }
    arr
}
pub fn result(i: usize, p: u64, n: u64) -> u64 {
    if p & n != 0 {
        return 0;
    }
    let temp = {
        let t = (n >> i) + 2;
        let t = t & t.wrapping_neg();
        if t & p >> i == 0 {
            0
        } else {
            (t - 2) << i
        }
    };
    temp | {
        let t = !n & ((1 << i) - 1);
        let t = t | t >> 1;
        let t = t | t >> 2;
        let t = t | t >> 4;
        let t = (t + 1) >> 1;
        if t & p == 0 {
            0
        } else {
            (1 << i) - (t << 1)
        }
    }
}
