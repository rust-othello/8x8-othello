use build_const::ConstWriter;
mod build_functions;

fn main() {
    // use `for_build` in `build.rs`
    let consts = //ConstWriter::for_build("constants").unwrap();
        ConstWriter::from_path(&std::path::Path::new("./src/constants.rs")).unwrap();
    let mut consts = consts.finish_dependencies();

    let mask: Vec<[[u64; 2]; 4]> = (0..64).map(build_functions::mask).collect();

    consts.add_array("MASK", "[[u64;2];4]", &mask);

    let index: Vec<[u16; 4]> = (0..64).map(build_functions::index).collect();

    consts.add_array("INDEX", "[u16;4]", &index);

    let result = build_functions::result_array();

    consts.add_array("RESULT", "u8", &result);
}
